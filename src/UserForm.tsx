import React from 'react';

import { Form, Field } from 'react-final-form';
import Styles from './Styles' 

const sleep = (ms: any) => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async (values: any) => {
  await sleep(300)
  window.alert(JSON.stringify(values, null, '\t'))
}

const required = (value: any) => (value ? undefined : 'Required')

function UserForm() {
    return (
        <Styles>
            <Form
                onSubmit={onSubmit}
                initialValues={{ stooge: 'larry', employed: false }}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                    <form onSubmit={handleSubmit}>
                        <Field name="firstName" validate={required}>
                                {({ input, meta }) => (
                                <div>
                                    <label>First Name</label>
                                    <input {...input} type="text" placeholder="First Name" />
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                </div>
                            )}
                        </Field>
                        
                        <Field name="lastName" validate={required}>
                                {({ input, meta }) => (
                                <div>
                                    <label>Last Name</label>
                                    <input {...input} type="text" placeholder="Last Name" />
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                </div>
                            )}
                        </Field>
                        <div>
                            <label>Age</label>
                            <Field
                                name="age"
                                component="input"
                                type="text"
                                placeholder="Age"
                            />
                        </div>
                        <div>
                            <label>Employed</label>
                            <Field name="employed" component="input" type="checkbox" />
                        </div>
                        <div>
                            <label>Favorite Color</label>
                            <Field name="favoriteColor" component="select">
                                <option />
                                <option value="#ff0000">Red</option>
                                <option value="#00ff00">Green</option>
                                <option value="#0000ff">Blue</option>
                            </Field>
                        </div>
                        <div>
                            <label>Sauces</label>
                            <div>
                                <label>
                                    <Field
                                        name="sauces"
                                        component="input"
                                        type="checkbox"
                                        value="ketchup"
                                    />{' '}
                                    Ketchup
                                </label>
                                <label>
                                    <Field
                                        name="sauces"
                                        component="input"
                                        type="checkbox"
                                        value="mustard"
                                    />{' '}
                                    Mustard
                                </label>
                                <label>
                                    <Field
                                        name="sauces"
                                        component="input"
                                        type="checkbox"
                                        value="mayonnaise"
                                    />{' '}
                                    Mayonnaise
                                </label>
                                <label>
                                    <Field
                                        name="sauces"
                                        component="input"
                                        type="checkbox"
                                        value="guacamole"
                                    />{' '}
                                    Guacamole 🥑
                                </label>
                            </div>
                        </div>
                        <div>
                            <label>Best Stooge</label>
                            <div>
                                <label>
                                    <Field
                                        name="stooge"
                                        component="input"
                                        type="radio"
                                        value="larry"
                                    />{' '}
                                    Larry
                                </label>
                                <label>
                                    <Field
                                        name="stooge"
                                        component="input"
                                        type="radio"
                                        value="moe"
                                    />{' '}
                                    Moe
                                </label>
                                <label>
                                    <Field
                                        name="stooge"
                                        component="input"
                                        type="radio"
                                        value="curly"
                                    />{' '}
                                    Curly
                                </label>
                            </div>
                        </div>
                        <div>
                            <label>Notes</label>
                            <Field name="notes" component="textarea" placeholder="Notes" />
                        </div>
                        <div className="buttons">
                            <button type="submit" disabled={submitting || pristine}>
                                Submit
                            </button>
                            <button
                                type="button"
                                onClick={form.reset}
                                disabled={submitting || pristine}
                            >
                                Reset
                            </button>
                        </div>
                        <pre>{JSON.stringify(values, null, '\t')}</pre>
                    </form>
                )}
            />
        </Styles>
    );
}

export default UserForm;